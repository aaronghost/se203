# SE203

## Structure du dépôt

* Le TD correspond à la programmation sur le Cortex-M4 en bare-metal et permet d'effectuer les tâches suivantes :
    * Allumage d'un sheld de LED à partir d'images données par l'UART
    * Système d'interruption pour limiter l'attente active sur les bus de communication
    * Implémentation de la communication sur le bus I2C
    * Implémentation de la communication avec l'accéléromètre (bus I2C)

* Le programme final choisi permet de jouer à Snake sur la carte.

* Les autres exercices sont installés dans les sous-répertoires du dossier Exercices.

## Liens vers les exercices

* Résumé des exercices de cross-compilation : [exercices de compilation](Exercices/Exercices%20Cross-compilation/resume.md)
* Makefile de l'exercice de makefile : [makefile](Exercices/Exercice%20makefile/makefile)
