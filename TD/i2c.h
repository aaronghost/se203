#ifndef I2C_H
#define I2C_H

#include <stdint.h>
#include "stm32l475xx.h"
#include "led.h"
#include "uart.h"

void i2c_init();
void i2c_write_bytes(uint8_t * bytes, uint8_t size);
uint8_t i2c_read_byte(uint8_t address);

#endif //I2C_
