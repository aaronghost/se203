#include "led.h"
#include "clocks/clocks.h"
#include "uart.h"
#include "matrix.h"
#include "irq.h"
#include "buttons.h"
#include "i2c.h"
#include "lsm6dsm.h"
#include "string.h"
#include "rng.h"
#include "snake.h"

//activate the different leds from the card in a cycle
int color_cycle(){
  led_init();
  led_g_on();
  for(;;){
    led(LED_OFF);
    for(int i = 0 ; i < 1000000 ; i++){
        asm volatile("nop");
    }
    led(LED_YELLOW);
    for(int i = 0 ; i < 1000000 ; i++){
        asm volatile("nop");
    }
    led(LED_BLUE);
    for(int i = 0 ; i < 1000000 ; i++){
        asm volatile("nop");
    }
  }
}

//echo on the uart
void echo(){
  NVIC_DisableIRQ(USART1_IRQn);
  while(1){
    uint8_t var[100];
    uint8_t text[15] = "Tu as écrit";
    uart_gets(var, 100);
    uart_puts(text);
    uart_puts(var);
  }
}

//function to test the accelerometer (communicates on the UART)
void test_accelerometer(){
  int16_t* accelerometer_datas;
  while(1){
    accelerometer_datas = read_accelerometer();
    direction dir = compute_direction(accelerometer_datas);
    if(dir == NORTH){
      uart_putchar('N');
      uart_putchar('\n');
    }else if(dir == SOUTH){
      uart_putchar('S');
      uart_putchar('\n');
    }else if(dir == WEST){
      uart_putchar('W');
      uart_putchar('\n');
    }else{
      uart_putchar('E');
      uart_putchar('\n');
    }
  }
}

//function to call with the python script to check the communication with the UART
int checksum(){
  uint32_t sum = 0;
  int i = 0;
  while(i < 1000){
    uint8_t tempvar = 0;
    tempvar = uart_getchar();
    sum += tempvar;
    i++;
  }
  led_g_on();
  return sum;
}



//possibles functions to execute are :
//color_cycle : led cycle on the leds from the card
//echo : uart function to test
//checksum : function with the python uart script
//test_pixels : draw lines alternating on the shield
//draw_static_image : draw a static image from the binary given
//draw_global_image : draw a global image received from the uart
//test_accelerometer : test the orientation of the accelerometer (sent on the uart)
//snake : play the snake
int main(){
  //all the initializations needed
  clocks_init();
  led_init();
  led_g_off();
  uart_init(38400);
  matrix_init();
  irq_init();
  button_init();
  lsm6dsm_init();
  rng_init();
  //choice of the function to execute
  snake_init();
  uint8_t result_of_the_turn = 1;
  while(result_of_the_turn){
    for(int i = 0 ; i < 500 ; i++){
      draw_frame();
    }
    result_of_the_turn = play_a_turn();
  }
  while(1){}
}
