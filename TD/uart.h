#ifndef UART_H
#define UART_H

#include <stddef.h>
#include "stm32l475xx.h"
#include "matrix.h"

//extern global variable for the image (not necessary due to the inclusion of matrix.h neeeded for the definition of rgb_color)
extern rgb_color colorTable[64];

//functions
void uart_init(int number);
void USART1_IRQHandler(void);
void uart_putchar(uint8_t c);
uint8_t uart_getchar();
void uart_puts(const uint8_t *s);
void uart_gets(uint8_t *s, size_t size);

#endif //UART_H
