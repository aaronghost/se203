#include "matrix.h"

void matrix_init(){
  //Enable the clocks for the three GPIOs
  RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOAEN | RCC_AHB2ENR_GPIOBEN | RCC_AHB2ENR_GPIOCEN;
  //Set the GPIO in high speed mode
  //GPIOA high speed mode
  GPIOA -> OSPEEDR = (GPIOA -> OSPEEDR & ~(GPIO_OSPEEDR_OSPEED2_0 | GPIO_OSPEEDR_OSPEED3_0
                                    | GPIO_OSPEEDR_OSPEED4_0 | GPIO_OSPEEDR_OSPEED5_0
                                    | GPIO_OSPEEDR_OSPEED6_0 | GPIO_OSPEEDR_OSPEED7_0 |
                                     GPIO_OSPEEDR_OSPEED15_0))
                                    | GPIO_OSPEEDR_OSPEED2_1 | GPIO_OSPEEDR_OSPEED3_1
                                    | GPIO_OSPEEDR_OSPEED4_1 | GPIO_OSPEEDR_OSPEED5_1
                                    | GPIO_OSPEEDR_OSPEED6_1 | GPIO_OSPEEDR_OSPEED7_1
                                    | GPIO_OSPEEDR_OSPEED15_1;
  //GPIOB high speed mode
  GPIOB -> OSPEEDR = (GPIOB -> OSPEEDR & ~(GPIO_OSPEEDR_OSPEED0_0 | GPIO_OSPEEDR_OSPEED1_0
                                    | GPIO_OSPEEDR_OSPEED2_0))
                                    | GPIO_OSPEEDR_OSPEED0_1 | GPIO_OSPEEDR_OSPEED1_1
                                    | GPIO_OSPEEDR_OSPEED2_1;
  //GPIOC high speed mode
  GPIOC -> OSPEEDR = (GPIOC -> OSPEEDR & ~(GPIO_OSPEEDR_OSPEED3_0 | GPIO_OSPEEDR_OSPEED4_0
                                    | GPIO_OSPEEDR_OSPEED5_0))
                                    | GPIO_OSPEEDR_OSPEED3_1 | GPIO_OSPEEDR_OSPEED4_1
                                    | GPIO_OSPEEDR_OSPEED5_1;
  //Set output mode to all the needed GPIOs
  //Set output mode to all the GPIOA needed interfaces
  GPIOA -> MODER = (GPIOA -> MODER & ~(GPIO_MODER_MODE2_1 | GPIO_MODER_MODE3_1
                                    | GPIO_MODER_MODE4_1 | GPIO_MODER_MODE5_1
                                    | GPIO_MODER_MODE6_1 | GPIO_MODER_MODE7_1 |
                                     GPIO_MODER_MODE15_1))
                                    | GPIO_MODER_MODE2_0 | GPIO_MODER_MODE3_0
                                    | GPIO_MODER_MODE4_0 | GPIO_MODER_MODE5_0
                                    | GPIO_MODER_MODE6_0 | GPIO_MODER_MODE7_0
                                    | GPIO_MODER_MODE15_0;
  //Set output mode to all the GPIOB needed interfaces
  GPIOB -> MODER = (GPIOB -> MODER & ~(GPIO_MODER_MODE0_1 | GPIO_MODER_MODE1_1
                                    | GPIO_MODER_MODE2_1))
                                    | GPIO_MODER_MODE0_0 | GPIO_MODER_MODE1_0
                                    | GPIO_MODER_MODE2_0;
  //Sets output mode to all the GPIOC needed interfaces
  GPIOC -> MODER = (GPIOC -> MODER & ~(GPIO_MODER_MODE3_1 | GPIO_MODER_MODE4_1
                                    | GPIO_MODER_MODE5_1))
                                    | GPIO_MODER_MODE3_0 | GPIO_MODER_MODE4_0
                                    | GPIO_MODER_MODE5_0;
  //Acceptable values for the driver
  //Set RST to 0
  RST(0);
  //GPIOC -> BSRR = GPIO_BSRR_BR3;

  //Set LAT to 1
  LAT(1);
  //GPIOC -> BSRR = GPIO_BSRR_BS4;

  //Set SB to 1
  SB(1);
  //GPIOC -> BSRR = GPIO_BSRR_BS5;

  //set SCK and SDA to 0
  SCK(0);
  SDA(0);
  //GPIOB -> BSRR = GPIO_BSRR_BR1;
  //GPIOA -> BSRR = GPIO_BSRR_BR4;
  //Set C0 to C7 to 0
  deactivate_rows();
  //Waiting
  for (int i=0; i<8000000; i++)
    asm volatile("nop");
  //Set RST to 1
  RST(1);
  //GPIOC -> BSRR = GPIO_BSRR_BS3;
  init_bank0();
}

void deactivate_rows(){
  ROW0(0);
  ROW1(0);
  ROW2(0);
  ROW3(0);
  ROW4(0);
  ROW5(0);
  ROW6(0);
  ROW7(0);
}

void activate_row(int row){
  switch(row){
    case(0):
      ROW0(1);
      break;
    case(1):
      ROW1(1);
      break;
    case(2):
      ROW2(1);
      break;
    case(3):
      ROW3(1);
      break;
    case(4):
      ROW4(1);
      break;
    case(5):
      ROW5(1);
      break;
    case(6):
      ROW6(1);
      break;
    default:
      ROW7(1);
      break;
  }
}

void send_byte(uint8_t val, int bank){
  //Select the bank
  SB(bank);
  for(int i = (bank == 0) ? 5 : 7; i >= 0 ; i--){
    //Shifts the bit to the right and take only the first bit
    SDA((val & (1 << i)));
    pulse_SCK();
  }
}

void mat_set_row(int row, const rgb_color *val){
  for(int i = 7 ; i >= 0 ; i--){
    send_byte(val[i].b,1);
    send_byte(val[i].g,1);
    send_byte(val[i].r,1);
  }
  deactivate_rows();
  for(int i =0 ; i <= 70; i++){
    asm volatile("nop");
  }
  pulse_LAT();
  activate_row(row);

}

void init_bank0(){
    for(int i = 24 ; i >= 0 ; i--){
      send_byte(0xff,0);
    }
  pulse_LAT();
}

void test_pixels(){
  rgb_color pixels;
  int i = 0;
  int color = 0;
  while(1){
    rgb_color line[8];
    int intensity = 255;
    //decrements the intensity from the beginning of the line to the end
    for(int position = 0 ; position <=7 ; position ++ ){
      pixels.r = (color == 0) ? intensity : 0;
      pixels.g = (color == 1) ? intensity : 0;
      pixels.b = (color == 2) ? intensity : 0;
      line[position] = pixels;
      intensity -= 32;
    }
    //set the raw
    mat_set_row(i,line);
    //waiting
    for (int i=0; i<10000000; i++)
      asm volatile("nop");
    i = (i+ 1) % 8;
    //change the color
    color = (color+1) % 3;
  }
}

void draw_static_image(){
  //Get the image
  uint8_t* binaryPointer = &_binary_image_raw_start;
  //Write the image in a rgb-color formatted table (the global variable also accessible from the UART)
  for(int i = 0; i < 8; i++){
    for(int j = 0; j < 8; j++){
      colorTable[i*8+j].r = *binaryPointer;
      binaryPointer++;
      colorTable[i*8+j].g = *binaryPointer;
      binaryPointer++;
      colorTable[i*8+j].b = *binaryPointer;
      binaryPointer++;
    }
  }
  //draw the image
  draw_global_image();
}

void draw_frame(){
  for(int lineIndex = 0 ; lineIndex <= 7 ; lineIndex ++){
    mat_set_row(lineIndex,&colorTable[lineIndex*8]);
  }
}

//draw the global image contained in the image
void draw_global_image(){
  while(1){
    for(int lineIndex = 0 ; lineIndex <= 7 ; lineIndex ++){
      mat_set_row(lineIndex,&colorTable[lineIndex*8]);
    }
  }
}
