#ifndef LSM6DSM_H
#define LSM6DSM_H

#include <stdint.h>
#include "stm32l475xx.h"
#include "i2c.h"
#include "led.h"

#define ldm6dsmAddress 0b11010100
#define whoAmI         0x0f
#define CTRL1_XL       0x10
#define INT1_CTRL      0x0d
#define STATUS_REG     0x1e
#define OUTX_L_XL      0x28
#define OUTX_H_XL      0x29
#define OUTY_L_XL      0x2a
#define OUTY_H_XL      0x2b

typedef enum{NORTH,SOUTH,EAST,WEST}direction;

void lsm6dsm_init();
int16_t * read_accelerometer();
direction compute_direction(int16_t * xy_acceleration);
direction get_direction();

#endif //LSM6DSM_H
