.syntax unified
.global _start,_exit
.thumb
.section .init

  .thumb_func
_start:
  LDR sp, =__stack_end__
  BL copy_data
  BL init_bss
  BL   main

_exit:
  B   _exit
