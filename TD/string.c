#include "string.h"

unsigned char* itoa(int16_t num, unsigned char * str)
{
    int i = 0;
    int16_t pow = 10000;
    uint8_t empty = 1;
    if (num == 0)
    {
        str[i++] = '0';
        return str;
    }else if (num < 0){
        str[i++] = '-';
        num = -num;
    }
    while (pow != 0)
    {
        uint8_t digit = num / pow;
        if(empty){
          //look for the first non-zero digit
          if(digit != 0){
              empty = 0;
          }
        }
        if(!empty){
          str[i++] = digit + '0';
        }
        num = num % pow;
        pow /= 10;
    }
    return str;
}
