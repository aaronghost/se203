#ifndef LED_H
#define  LED_H

#include <stdint.h>
#include "stm32l475xx.h"

//struct to define the types of the LEDs 3 and 4
typedef enum {LED_OFF, LED_YELLOW, LED_BLUE}state;

//functions of the led
void led_init();
void led_g_on();
void led(state newState);
void led_g_off();

#endif //LED_H
