#include "rng.h"

void rng_init(){
  //Select the PLLSAI1 as the 48MHz clk
  RCC -> AHB2ENR |= RCC_AHB2ENR_RNGEN;
  RNG -> CR |= RNG_CR_RNGEN;
}

uint32_t get_random_number(){
  while(!(RNG -> SR >> RNG_SR_DRDY_Pos & 1U)){

  }
  return RNG -> DR;
}
