#include "uart.h"

void uart_init(int baudrate){
  //////////// UART initialization ////////////
  RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
  //GPIOB port B6 : AF7 USART1_TX
  GPIOB -> AFR[0] = (GPIOB -> AFR[0] & ~ GPIO_AFRL_AFSEL6_3 )| GPIO_AFRL_AFSEL6_2 | GPIO_AFRL_AFSEL6_1 | GPIO_AFRL_AFSEL6_0;
  GPIOB -> MODER = (GPIOB -> MODER & ~GPIO_MODER_MODE6_0) | GPIO_MODER_MODE6_1;
  //GPIOB port B7: AF7 USART1_RX
  GPIOB -> AFR[0] = (GPIOB -> AFR[0] & ~ GPIO_AFRL_AFSEL7_3 )| GPIO_AFRL_AFSEL7_2 | GPIO_AFRL_AFSEL7_1 | GPIO_AFRL_AFSEL7_0;
  GPIOB -> MODER =(GPIOB -> MODER & ~GPIO_MODER_MODE7_0) | GPIO_MODER_MODE7_1;
  //clock of the USART1
  RCC -> APB2ENR |= RCC_APB2ENR_USART1EN;
  //specify the clock
  RCC -> CCIPR &= ~ RCC_CCIPR_USART1SEL;
  //reset the serial port
  RCC -> APB2RSTR |= RCC_APB2RSTR_USART1RST;
  RCC -> APB2RSTR &= ~RCC_APB2RSTR_USART1RST;
  //Set the baud rate (Cplk frequency/ baud)
  USART1 -> BRR |= 80000000/baudrate;
  //Oversampling at 16
  USART1 -> CR3 = 0;
  USART1 -> CR1 = 0;

  //////////// UART IRQ initialization ////////////
  NVIC_EnableIRQ(USART1_IRQn);
  USART1 -> CR1 |= USART_CR1_RXNEIE;
  USART1 -> CR3 |= USART_CR3_EIE;

  //////////// UART enabling ////////////
  //enabling the uart, transmission and reception
  USART1 -> CR1 |= USART_CR1_UE;
  USART1 -> CR1 |= USART_CR1_TE;
  USART1 -> CR1 |= USART_CR1_RE;
  //waiting the completion of the enabling
  while(!((USART1-> ISR >> USART_ISR_TEACK_Pos)& 1U)){

  }
  while(!((USART1-> ISR >> USART_ISR_REACK_Pos)& 1U)){

  }
}

void USART1_IRQHandler(){
  //declaration of the statics variables used in the interruption
  static uint8_t index = 0;
  static uint8_t color = 0;
  static uint8_t ignoreFrame = 0;
  //On failure ignore frame
  if(((USART1 -> ISR >> USART_ISR_ORE_Pos) & 1U) || ((USART1 -> ISR >> USART_ISR_FE_Pos) & 1U)){
    ignoreFrame = 1;
  }

  //Check that the interruption is related to the reception of a char
  if((USART1 -> ISR >> USART_ISR_RXNE_Pos) & 1U){
    uint8_t newColor = uart_getchar();
    //New frame character
    if(newColor == 0xff){
      index = 0;
      color = 0;
      ignoreFrame = 0;
      return;
    }
    //Return if the frame is ignored
    if(ignoreFrame == 1){
      return;
    }
    //Read a single byte and set it to the corresponding color in the complete frame
    switch(color){
      case 0:
        colorTable[index].r = newColor;
        break;
      case 1:
        colorTable[index].g = newColor;
        break;
      case 2:
        colorTable[index].b = newColor;
        index++;
        break;
     }
     color = (color + 1) % 3;
  }
}

void uart_putchar(uint8_t c){
  while(!((USART1 -> ISR >> USART_ISR_TXE_Pos) & 1U)){
  }
  USART1 -> TDR = c;
}

uint8_t uart_getchar(){
  while(!((USART1 -> ISR >> USART_ISR_RXNE_Pos) & 1U)){

  }
  return USART1 -> RDR;
}

void uart_puts(const uint8_t *s){
  while(!((USART1 -> ISR >> USART_ISR_TC_Pos) & 1U)){
  }
  int i = 0;
  while(s[i]){
    uart_putchar(s[i]);
    i++;
  }
  uart_putchar('\r');
  uart_putchar('\n');
}

void uart_gets(uint8_t *s, size_t size){
  size_t i = 0;
  while(i < size-1){
    uint8_t rep = uart_getchar();
    s[i]= rep;
    i++;
    if(rep == '\r' || rep == '\n'){
      break;
    }
  }
  s[i] = 0;
}
