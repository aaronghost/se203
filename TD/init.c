#include <stdint.h>

//vam references
extern uint8_t __bss_start__, __bss_end__;
extern uint8_t __data_start__, __data_end__;
extern uint8_t __text_start__, __text_end__;
extern uint8_t __vtable_start__, __vtable_end__;

//lam references
extern uint8_t __vtable_lam_start__,__text_lam_start__, __data_lam_start__;

//internal copy function to raw memory copy
void __attribute__((section(".init"))) copy_to_ram(uint8_t* VMA_address_start, uint8_t* VMA_address_end, uint8_t* LMA_address_start){
  uint8_t* src = LMA_address_start;
  for(uint8_t* dst = VMA_address_start; dst < VMA_address_end; dst++){
    *dst = *src;
    src++;
  }
}

//copy of the different sections from the flash to the ram
void __attribute__((section(".init"))) copy_data(){
  copy_to_ram(&__vtable_start__, &__vtable_end__, &__vtable_lam_start__);
  copy_to_ram(&__text_start__, &__text_end__, &__text_lam_start__);
  copy_to_ram(&__data_start__, &__data_end__, &__data_lam_start__);
}

//fills the bss with 0
void __attribute__((section(".init"))) init_bss(){
    for(uint8_t* src = &__bss_start__; src < &__bss_end__; src++){
      *src = 0;
    }
}
