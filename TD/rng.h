#ifndef RNG_H
#define RNG_H

#include <stdint.h>
#include "stm32l475xx.h"
#include "led.h"

void rng_init();
uint32_t get_random_number();

#endif //RNG_H
