#include "lsm6dsm.h"

void lsm6dsm_init(){
  i2c_init();
  //enabling the accelerometer
  uint8_t ctrTable[2] = {CTRL1_XL, 0x60};
  i2c_write_bytes(ctrTable,2);
  uint8_t intTable[2] = {INT1_CTRL, 0x01};
  i2c_write_bytes(intTable,2);
}

int16_t * read_accelerometer(){
  static int16_t xy_acceleration[2];
  if(!(i2c_read_byte(STATUS_REG) & 1)){
    return xy_acceleration;
  }
  xy_acceleration[0] = ((int16_t)i2c_read_byte(OUTX_H_XL)<< 8) | i2c_read_byte(OUTX_L_XL);
  xy_acceleration[1] = ((int16_t)i2c_read_byte(OUTY_H_XL)<< 8) | i2c_read_byte(OUTY_L_XL);
  return xy_acceleration;
}

direction compute_direction(int16_t * xy_acceleration){
  int16_t x = xy_acceleration[0];
  int16_t y = xy_acceleration[1];
  if(x >= y && x >= -y){
      return NORTH;
  }else if(x < -y && x < y){
      return SOUTH;
  }else if(y > 0){
      return WEST;
  }else{
      return EAST;
  }
}

direction get_direction(){
  return compute_direction(read_accelerometer());
}
