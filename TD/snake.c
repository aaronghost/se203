#include "snake.h"

static uint8_t logicTable[8][8];
static position snakeTable[64];
static uint8_t length = 1;
static position apple;

void snake_init(){
  position snake_head;
  snake_head.x = 4;
  snake_head.y = 4;
  logicTable[snake_head.x][snake_head.y] = 1;
  length = 1;
  snakeTable[0]=snake_head;
  uint32_t random_number = get_random_number();
  apple.x = ((uint8_t) random_number) % 8;
  apple.y = ((uint8_t) (random_number >> 8)) % 8;
  logicTable[apple.x][apple.y] = 2;
  set_graphics();
}

uint8_t play_a_turn(){
  //The field is complete the game is won
  if(length == 64){
    return 2;
  }
  position new_pos = calculate_next_position();
  //check if the position is valid
  //out of the box
  if(new_pos.x >= 8 || new_pos.x <= -1 || new_pos.y >= 8 || new_pos.y <= -1){
    led(LED_YELLOW);
    return 0;
  }
  //check if the snake is eating itself
  if(logicTable[new_pos.x][new_pos.y] == 1){
    led(LED_BLUE);
    return 0;
  }
  uint8_t ate_an_apple = 0;
  if(logicTable[new_pos.x][new_pos.y] == 2){
    ate_an_apple = 1;
  }
  if(ate_an_apple == 1){
    length++;
    uint8_t correct_new_pos = 1;
    position new_apple;
    //apple position validation
    uint8_t decal = 0;
    do{
      correct_new_pos = 1;
      //TODO use random generator
      uint32_t random_number;
      if(decal == 0){
        random_number = get_random_number();
      }
      new_apple.x = ((uint8_t) random_number >> (3*decal)) % 8;
      new_apple.y = ((uint8_t) (random_number >> (3 + 3*decal))) % 8;
      decal = (decal + 1) % 10;
      //prevent double apple position
      if(new_apple.x == apple.x && new_apple.y == apple.y){
        correct_new_pos = 0;
      //prevent from creating an apple in the snake
      }else if(logicTable[new_apple.x][new_apple.y] == 1){
        correct_new_pos = 0;
      }
    }while(!correct_new_pos);
    apple = new_apple;
    logicTable[apple.x][apple.y]=2;
  }else{
    logicTable[snakeTable[length-1].x][snakeTable[length-1].y] = 0;
    position empty;
    empty.x = 0;
    empty.y = 0;
    snakeTable[length-1] = empty;
  }
  for(int snake_counter = length-2; snake_counter >= 0 ; snake_counter--){
    snakeTable[snake_counter+1] = snakeTable[snake_counter];
  }
  snakeTable[0] = new_pos;
  logicTable[snakeTable[0].x][snakeTable[0].y]=1;
  set_graphics();
  return 1;
}

void set_graphics(){
  for(int x = 0 ; x < 8 ; x++){
    for(int y = 0 ; y < 8 ; y++){
      if(logicTable[x][y] == 1){
        colorTable[x*8+y].r = 0xff;
        colorTable[x*8+y].g = 0x00;
        colorTable[x*8+y].b = 0x00;
      }else if(logicTable[x][y] == 2){
        colorTable[x*8+y].r = 0x00;
        colorTable[x*8+y].g = 0xff;
        colorTable[x*8+y].b = 0x00;
      }else{
        colorTable[x*8+y].r = 0x00;
        colorTable[x*8+y].g = 0x00;
        colorTable[x*8+y].b = 0x00;
      }
    }
  }
}

position calculate_next_position(){
  direction dir = get_direction();
  //calculate the new position
  position new_pos;
  new_pos.x = snakeTable[0].x;
  new_pos.y = snakeTable[0].y;
  if(dir == NORTH){
     new_pos.x--;
     uart_putchar('N');
     uart_putchar('\n');
  }else if(dir == SOUTH){
    new_pos.x++;
    uart_putchar('S');
    uart_putchar('\n');
  }else if(dir == WEST){
    new_pos.y--;
    uart_putchar('W');
    uart_putchar('\n');
  }else{
    new_pos.y++;
    uart_putchar('E');
    uart_putchar('\n');
  }
  return new_pos;
}
