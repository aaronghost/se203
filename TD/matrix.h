#ifndef MATRIX_H
#define MATRIX_H

#include "stm32l475xx.h"
#include <stdint.h>

//MACROs for the control of the different pins of the driver
#define RST(x)  do{GPIOC -> BSRR = (x == 0) ? GPIO_BSRR_BR3 : GPIO_BSRR_BS3;}while(0)
#define SB(x)   do{GPIOC -> BSRR = (x == 0) ? GPIO_BSRR_BR5 : GPIO_BSRR_BS5;}while(0)
#define LAT(x)  do{GPIOC -> BSRR = (x == 0) ? GPIO_BSRR_BR4 : GPIO_BSRR_BS4;}while(0)
#define SCK(x)  do{GPIOB -> BSRR = (x == 0) ? GPIO_BSRR_BR1 : GPIO_BSRR_BS1;}while(0)
#define SDA(x)  do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR4 : GPIO_BSRR_BS4;}while(0)
#define ROW0(x) do{GPIOB -> BSRR = (x == 0) ? GPIO_BSRR_BR2 : GPIO_BSRR_BS2;}while(0)
#define ROW1(x) do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR15 : GPIO_BSRR_BS15;}while(0)
#define ROW2(x) do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR2 : GPIO_BSRR_BS2;}while(0)
#define ROW3(x) do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR7 : GPIO_BSRR_BS7;}while(0)
#define ROW4(x) do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR6 : GPIO_BSRR_BS6;}while(0)
#define ROW5(x) do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR5 : GPIO_BSRR_BS5;}while(0)
#define ROW6(x) do{GPIOB -> BSRR = (x == 0) ? GPIO_BSRR_BR0 : GPIO_BSRR_BS0;}while(0)
#define ROW7(x) do{GPIOA -> BSRR = (x == 0) ? GPIO_BSRR_BR3 : GPIO_BSRR_BS3;}while(0)

//MACROs generating pulses
#define pulse_SCK() do{SCK(0); asm volatile("nop"); asm volatile("nop"); SCK(1); asm volatile("nop"); asm volatile("nop"); SCK(0); asm volatile("nop");asm volatile("nop");}while(0)
#define pulse_LAT() do{LAT(1); asm volatile("nop"); asm volatile("nop"); LAT(0); asm volatile("nop"); asm volatile("nop"); LAT(1); asm volatile("nop");asm volatile("nop");}while(0)

//RGB color
typedef struct {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} rgb_color;

//extern variable for the image from the binary
extern uint8_t _binary_image_raw_start;

//global variable for the frame
rgb_color colorTable[64];

//Functions
void matrix_init();
void deactivate_rows();
void activate_row(int row);
void send_byte(uint8_t val, int bank);
void mat_set_row(int row, const rgb_color *val);
void init_bank0();
void test_pixels();
void draw_static_image();
void draw_frame();
void draw_global_image();

#endif //MATRIX_H
