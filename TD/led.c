#include "led.h"

void led_init(){
  //Set the clocks for GPIOB and GPIOC
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
  RCC->AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
  //Set the GPIOB_Moder
  GPIOB -> MODER = (GPIOB -> MODER &  ~GPIO_MODER_MODE14_1)  | GPIO_MODER_MODE14_0;
  return;
}

void led_g_on(){
  GPIOB -> BSRR = GPIO_BSRR_BS14;
  return;
}

void led(state newState){
  switch(newState){
    case(LED_OFF):
      //Force the PC9 in input mode (two registers at 0)
      GPIOC -> MODER &= ~GPIO_MODER_MODE9;
      break;
    case(LED_YELLOW):
      //Force the PC9 in output mode
      GPIOC -> MODER =(GPIOC -> MODER & ~GPIO_MODER_MODE9_1)| GPIO_MODER_MODE9_0;
      //Set on Led2 and reset on Led3
      GPIOC -> BSRR = GPIO_BSRR_BS9;
      GPIOB -> BSRR = GPIO_BSRR_BR14;
      break;
    case(LED_BLUE):
      //Force the PC9 in output mode
      GPIOC -> MODER =(GPIOC -> MODER & ~GPIO_MODER_MODE9_1)| GPIO_MODER_MODE9_0;
      //Set on Led2 and Reset on Led4
      GPIOB -> BSRR = GPIO_BSRR_BR14;
      GPIOC -> BSRR = GPIO_BSRR_BR9;
      break;
  }
  return;
}

void led_g_off(){
  GPIOB -> BSRR = GPIO_BSRR_BR14;
  return;
}
