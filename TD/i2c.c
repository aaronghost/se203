#include "i2c.h"

void i2c_init(){
  //Enable the GPIO (GPIOB 10 and 11) AF4 as the SCL and the SDA
  RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOBEN;
  GPIOB -> AFR[1] = (GPIOB -> AFR[1] & ~ ( GPIO_AFRH_AFSEL10_3 | GPIO_AFRH_AFSEL10_1 | GPIO_AFRH_AFSEL10_0)) | GPIO_AFRH_AFSEL10_2;
  GPIOB -> MODER = (GPIOB -> MODER & ~GPIO_MODER_MODE10_0) | GPIO_MODER_MODE10_1;
  GPIOB -> AFR[1] = (GPIOB -> AFR[1] & ~ ( GPIO_AFRH_AFSEL11_3 | GPIO_AFRH_AFSEL11_1 | GPIO_AFRH_AFSEL11_0)) | GPIO_AFRH_AFSEL11_2;
  GPIOB -> MODER = (GPIOB -> MODER & ~GPIO_MODER_MODE11_0) | GPIO_MODER_MODE11_1;
  //Enable a clock for the I2C2
  RCC -> APB1ENR1 |= RCC_APB1ENR1_I2C2EN;
  RCC -> APB1RSTR1 |= RCC_APB1RSTR1_I2C2RST;
  RCC -> APB1RSTR1 &= ~RCC_APB1RSTR1_I2C2RST;
  I2C2 -> CR1 &= ~I2C_CR1_PE;
  //Select the SYSCLK as the source of the I2C2 clk
  RCC -> CCIPR = (RCC -> CCIPR &= ~RCC_CCIPR_I2C2SEL_1) | RCC_CCIPR_I2C2SEL_0;
  //I2C_TIMINGR setting based on SCLH and SCLL
  //Prescale puts internal I2C2 at 125ns per instruction on the internal clk
  I2C2 -> TIMINGR = (9 << I2C_TIMINGR_PRESC_Pos) | (37 << I2C_TIMINGR_SCLL_Pos) | (31 << I2C_TIMINGR_SCLH_Pos)
                 | (15 << I2C_TIMINGR_SDADEL_Pos) | (1 << I2C_TIMINGR_SCLDEL_Pos);


  //I2C2_CR2 : Addressing mode at 7 bits on ADD10
  I2C2 -> CR2 &= ~ I2C_CR2_ADD10;
  //I2C2_CR2 : SADD (address of the slave) specific to the lsm6dsm : 1101010
  I2C2 -> CR2 |= (0b11010100 << I2C_CR2_SADD_Pos);
  //AUTOEND = 0  software end mode (permits restart condition)
  I2C2 -> CR2 &= ~ I2C_CR2_AUTOEND;
  I2C2 -> CR1 |= I2C_CR1_PE;

  //////Initial communication to have a callback with the address of the slave //////
  uint8_t whoAmIAddress = 0x0F;
  uint8_t byte = i2c_read_byte(whoAmIAddress);
  uart_putchar(byte);
}

uint8_t i2c_read_byte(uint8_t address_to_read){
  //Write a single write access
  uint8_t table[1] = {address_to_read};
  i2c_write_bytes(table, 1);
  //Read mode for the answer of the lsm6dsm
  I2C2 -> CR2 |= I2C_CR2_RD_WRN;
  //Put the nbytes to the new value (2 bytes to be read)
  I2C2 -> CR2 &= ~I2C_CR2_NBYTES;
  I2C2 -> CR2 |= (1 << I2C_CR2_NBYTES_Pos);
  //restart the communication
  I2C2 -> CR2 |= I2C_CR2_START;
  //Communication busy/ not ready
  while(!((I2C2-> ISR >> I2C_ISR_RXNE_Pos)& 1U)){
  }
  uint8_t ans = I2C2 -> RXDR;
  while(!((I2C2-> ISR >> I2C_ISR_TC_Pos)& 1U)){
  }
  return ans;
}

void i2c_write_bytes(uint8_t * bytes, uint8_t size){
  //transfer direction RD_WRN (write mode)
  I2C2 -> CR2 &=~ I2C_CR2_RD_WRN;
  //Prepare a one byte write access
  I2C2 -> CR2 &= ~I2C_CR2_NBYTES;
  I2C2 -> CR2 |= (size << I2C_CR2_NBYTES_Pos);
  //start the communication
  I2C2 -> CR2 |= I2C_CR2_START;
  for(uint8_t i = 0; i < size ; i++){
    //Communication busy/ not ready
    while(!((I2C2-> ISR >> I2C_ISR_TXIS_Pos)& 1U)){
    }
    I2C2 -> TXDR = bytes[i];
  }
  //Wait for acknowlegment (transmission finished)
  while(!((I2C2-> ISR >> I2C_ISR_TC_Pos)& 1U)){
  }
}
