#include "buttons.h"

void button_init(void){
  //clocks enabling
  RCC -> AHB2ENR |= RCC_AHB2ENR_GPIOCEN;
  //output mode
  GPIOC -> MODER &= ~(GPIO_MODER_MODE13);
  //unmask the interruption for the GPIO
  EXTI -> IMR1 |= EXTI_IMR1_IM13;
  //Set the interruption on failing edge
  EXTI -> FTSR1 |= EXTI_FTSR1_FT13;
  EXTI -> RTSR1 &= ~ EXTI_RTSR1_RT13;
  NVIC_EnableIRQ(EXTI15_10_IRQn);
  //Select the PC13 as the source of the interruption
  SYSCFG -> EXTICR[3] = SYSCFG_EXTICR4_EXTI13_PC;
}

void EXTI15_10_IRQHandler(){
  //acknolwedge treatment of the pending interruption
  EXTI -> PR1 |= EXTI_PR1_PIF13;
  //persistence of the state of the led
  static uint8_t isOn = 0;
  if(isOn== 0){
    led_g_on();
    isOn = 1;
  }else{
    led_g_off();
    isOn=0;
  }
  return;
}
