#ifndef SNAKE_H
#define SNAKE_H

#include <stdint.h>
#include "lsm6dsm.h"
#include "uart.h"
#include "led.h"
#include "rng.h"

typedef struct{
  int8_t x;
  int8_t y;
}position;

extern rgb_color colorTable[64];

void snake_init();
uint8_t play_a_turn();
void update_the_snake(position new_pos, uint8_t ate_an_apple);
position calculate_next_position();
void set_graphics();
position fibonnaci_random();

#endif //SNAKE_H
