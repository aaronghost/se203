#ifndef BUTTONS_H
#define BUTTONS_H

#include "stm32l475xx.h"
#include "led.h"

void EXTI15_10_IRQHandler(void);
void button_init(void);

#endif //BUTTONS_H
