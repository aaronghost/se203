# Exercices du slide de cross-compilation

## Exercice 1

* Réponse courte : __le premier chiffre n'indique pas l'adresse mais la longueur de l'objet lorsqu'il est dans la section \*COM\*__.

* Lien vers l'explication détaillée : [explication](Exercice1/explication.md)

## Exercice 2

* Réponses courtes :
    * Question 1 : __.text > .rodata > .data > .bss >  heap > stack__
    * Question 2 : __La pile croit de la fin de la zone mémoire de la pile vers le début__.

* Lien vers l'explication détaillée : [explication](Exercice2/explication.md)

## Exercice 3

* Pas de réponse courte

* Lien vers des explications (non-exhaustives) : [explication](Exercice3/explication.md)

* __L'exercice 3 est incomplet notamment la Question 3__
