# Exercice 1

Afin de comparer efficacement les différents types de variables dans la partie \*COM\* et les résultats de la commande ```arm-none-eabi-objdump -t main.o```, on peut modifier le code fourni précédemment pour ajouter des variables supplémentaires.

Le bloc de code suivant définit tous les types de variables de taille fixe et un int :
```c
#include <stdint.h>
#include <stdio.h>

int32_t x =34;
int8_t y;
int16_t a;
int32_t b;
int64_t c;
int d;
int main() {...}
```

On constate avec la commande précédemment citée les résultats suivants :
```
00000001       O *COM*	00000001 y
00000002       O *COM*	00000002 a
00000004       O *COM*	00000004 b
00000008       O *COM*	00000008 c
00000004       O *COM*	00000004 d
```
On peut donc en déduire que __le premier chiffre n'indique pas l'adresse mais la longueur de l'objet lorsqu'il est dans la section \*COM\*__.

NB : On constate de plus que dans ce cas les int sont stockés sur 32 bits.
