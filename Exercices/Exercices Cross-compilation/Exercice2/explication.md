# Exercice 2

## Question 1

* On modifie le code de l'exercice 1 pour faire appel au tas et à la pile :
```c
[...]
int forcestack(int a){
 return a+1;
}

int main() {
 [...]
 int* forceheap = malloc(sizeof(int));
 forcestack(2);
 [...]
 while(1){}
 return 0;
}
```

 ```gcc -c main.c```.

  On examine ensuite les sections avec la commande ```objdump -h main.o```
```
Sections :
Idx Name          Taille    VMA               LMA               Off fich  Algn
  0 .text         0000009c  0000000000000000  0000000000000000  00000040  2**0
                  CONTENTS, ALLOC, LOAD, RELOC, READONLY, CODE
  1 .data         00000004  0000000000000000  0000000000000000  000000dc  2**2
                  CONTENTS, ALLOC, LOAD, DATA
  2 .bss          00000001  0000000000000000  0000000000000000  000000e0  2**0
                  ALLOC
  3 .rodata       00000030  0000000000000000  0000000000000000  000000e0  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
  4 .comment      00000024  0000000000000000  0000000000000000  00000110  2**0
                  CONTENTS, READONLY
  5 .note.GNU-stack 00000000  0000000000000000  0000000000000000  00000134  2**0
                  CONTENTS, READONLY
  6 .eh_frame     00000038  0000000000000000  0000000000000000  00000138  2**3
                  CONTENTS, ALLOC, LOAD, RELOC, READONLY, DATA
```

__L'ordre est alors .text > .data > .bss > .rodata__

* On constate qu'en examinant un programme complet, on obtient plus de sections
```
main:     format de fichier elf64-x86-64

Sections :
Idx Name          Taille    VMA               LMA               Off fich  Algn
[...]
 13 .text         000001a1  0000000000001050  0000000000001050  00001050  2**4
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 14 .fini         00000009  00000000000011f4  00000000000011f4  000011f4  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, CODE
 15 .rodata       00000015  0000000000002000  0000000000002000  00002000  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 16 .eh_frame_hdr 00000044  0000000000002018  0000000000002018  00002018  2**2
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 17 .eh_frame     00000128  0000000000002060  0000000000002060  00002060  2**3
                  CONTENTS, ALLOC, LOAD, READONLY, DATA
 18 .init_array   00000008  0000000000003db8  0000000000003db8  00002db8  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 19 .fini_array   00000008  0000000000003dc0  0000000000003dc0  00002dc0  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 20 .dynamic      000001f0  0000000000003dc8  0000000000003dc8  00002dc8  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 21 .got          00000048  0000000000003fb8  0000000000003fb8  00002fb8  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 22 .data         00000014  0000000000004000  0000000000004000  00003000  2**3
                  CONTENTS, ALLOC, LOAD, DATA
 23 .bss          00000004  0000000000004014  0000000000004014  00003014  2**0
                  ALLOC
[...]
```

L'ordre est différent du fichier.o : __.text > .rodata > .data > .bss__

* On ne peut pas voir la position de la pile et du tas car ils n'existent pas dans l'objet ou le programme avant l'exécution.

* Afin d'avoir une idée de ce qui se passe à l’exécution, on lance le programme et en utilisant son pid on peut récupérer le mapping mémoire avec ```cat /opt/<pid>/maps``` ce qui donne le résultat suivant :

```
64e97567000-564e97568000  r--p 00000000 08:01 136753      /[...]/main
564e97568000-564e97569000 r-xp 00001000 08:01 136753      /[...]/main
564e97569000-564e9756a000 r--p 00002000 08:01 136753      /[...]/main
564e9756a000-564e9756b000 r--p 00002000 08:01 136753      /[...]/main
564e9756b000-564e9756c000 rw-p 00003000 08:01 136753      /[...]/main
564e99350000-564e99371000 rw-p 00000000 00:00 0           [heap]
[...]
7fe51abc3000-7fe51abc4000 rw-p 00000000 00:00 0
7ffdd9faf000-7ffdd9fd0000 rw-p 00000000 00:00 0           [stack]
7ffdd9feb000-7ffdd9fee000 r--p 00000000 00:00 0           [vvar]
7ffdd9fee000-7ffdd9fef000 r-xp 00000000 00:00 0           [vdso]
ffffffffff600000-ffffffffff601000 r-xp 00000000 00:00 0   [vsyscall]

```

* En examinant les droits d'accès sur la mémoire, on peut en déduire que la section r--xp correspond à la page mémoire dans laquelle se situe le .text (on parle de mémoire paginée par la MMU). Le mapping ne donne pas d'informations satisfaisantes sur les autres sections du programme elf mais on peut néanmoins en déduire la structure globale avec la pile et le tas : __.text > .rodata > .data > .bss >  heap > stack__

## Question 2

* Un premier indice sur la croissance de la pile et la présence d'une page mémoire vierge avant la page contentant la pile.

* Pour confirmer cette première impression, on peut utiliser gdb sur le programme issu du code présenté au début. On place deux breakpoints dans le code avant et pendant l'appel à la fonction forcestack. Ci-dessous, les lignes du programme c où sont situés les breakpoints et la valeur du rsp register associée

```
main.c 17 : rsp            0x7fffffffdd20
main.c 10 : rsp            0x7fffffffdd10
main.c 18 : rsp            0x7fffffffdd20
```

* On confirme donc que __la pile grossit de la fin de la zone mémoire allouée vers le début__.
