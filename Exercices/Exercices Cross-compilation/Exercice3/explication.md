# Exercice 3

## Question 1

### Optimisation -O0

```
Name          Size
.text         0000007b
.data         00000004
.bss          00000001
.rodata       00000040
```

* Le .bss correspond à : ```static uint8_t z```
* Le .data correspond à : ```static uint32_t y```
* Le .rodata est plus compliqué à décomposer :

```
              values                      printable values
0000 48656c6c 6f20576f 726c6421 0a004865  Hello World!..He
0010 6c6c6f20 576f726c 64210000 00000000  llo World!......
0020 78203d20 25642c20 79203d20 25642c20  x = %d, y = %d,
0030 7a203d20 25642c20 74203d20 25640a00  z = %d, t = %d..
```
* Les deux chaînes sont identiques à la différence du retour à la ligne dans la première chaîne de caractères. Les deux sont terminées par un zéro.
* La deuxième chaîne est celle qui est passée dans la fonction d'affichage puts (le retour à la ligne est ajoutée dans la fonction automatiquement.
* Il y a ensuite 10 octets vierges.
* Enfin on trouve la mise en forme pour l'affichage dans printf. La chaîne est aussi terminée par un zéro.

### Optimisation -O1

```
Name           Size
.text          0000005f
.data          00000004
.bss           00000001
.rodata.str1.1 0000000d
.rodata.str1.8 00000020
.rodata        0000000e
```
* La taille du .text a diminué et on trouve un partitionnement de .rodata avec .rodata.str1.*
* Comme les strings sont séparées du reste du .rodata, il n'y a plus le bloc d'octets vierges.
* La diminution du .text est lié à une optimisation dans le printf avec le code du main (appel à \_\_printf\_chk plutôt qu'à printf)


### Optimisation -O2

```
Name           Size
.text          00000000
.data          00000004
.bss           00000001
.rodata.str1.1 0000000d
.rodata.str1.8 00000020
.text.startup  00000059
.rodata        0000000e
```

* Pas de modifications sur les sections autres que .text qui est remplacée par .text.startup
* Deux instructions ```move $0x0,%eax``` ont été remplacées par ```xor %eax,%eax``` (différence de 3)
* Les appels de fonctions de stdio sont les mêmes mais des optimisations sont faites dans les premières lignes du code assembleur (padding réduit)

### Optimisation -Os

```
Name           Size
.text          00000000
.data          00000004
.bss           00000001
.rodata.str1.1 0000002d
.text.startup  00000058
.rodata        0000000e
```

* Les deux strings sont regroupées dans rodata.str1.1 mais il y a toujours le doublon de la chaîne de caractères pour puts
* Le code dans le .text.startup est le même que en -O2 à la différence de l'utilisation de %al à la place de %eax (registres de plus petite taille)

## Question 2

### Optimisation -O0

```
Name          Size
.text         0000007b
.data         00000004
.bss          00000001
.rodata       00000040
```

* Les tailles sont identiques, les .text sont identiques avec ou sans le mot clef static

### Optimisation -O1

```
Name           Size
.text          0000005f
.data          00000004
.bss           00000001
.rodata.str1.1 0000000d
.rodata.str1.8 00000020
```

* Pas de section .rodata (pas de doublon de la chaine de caractère)
* La chaîne de caractère qui reste est celle sans retour à la ligne

### Optimisation -O2

```
Name           Size
.text          00000000
.data          00000004
.bss           00000001
.rodata.str1.1 0000000d
.rodata.str1.8 00000020
.text.startup  00000059
```

* Mêmes avantages en terme d'économie de la donnée que -O1

### Optimisation -Os

```
Name           Size
.text          00000000
.data          00000004
.bss           00000001
.rodata.str1.1 0000002d
.text.startup  00000058
.rodata        0000000e
```

## Question 3

### Optimisation -O0



### Optimisation -O1

### Optimisation -O2

### Optimisation -Os
